package ictgradschool.industry.io.ex01;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class ExerciseOne {

    public void start() {



        printNumEsWithFileReader();

        printNumEsWithBufferedReader();

    }

    private void printNumEsWithFileReader() {

        int numE = 0;
        int total = 0;
        int num = 0;
        String message ="";
        try(FileReader fR = new FileReader("input2.txt")) {

            while ((num = fR.read()) != -1) {
                message = message + ((char) num);
            }
        }catch(FileNotFoundException e) {
            System.out.println("File not found brblem" + e);
        }catch(IOException e){
            System.out.println("IO problem" +e);
            }
            total = message.length();
        for (int i = 0; i < message.length() ; i++) {
            if (message.toLowerCase().charAt(i) == 'e'){
                numE++;
            }

        }
System.out.println(message);
        System.out.println("Number of e/E's: " + numE + " out of " + total);
    }

    private void printNumEsWithBufferedReader() {

        int numE = 0;
        int total = 0;
        int num = 0;
        String message = "";
        try (BufferedReader bR = new BufferedReader(new FileReader("input2.txt"))) {
            String line = null;
            while ((line = bR.readLine()) != null) {
                message += line;
            }
        } catch (IOException e) {
            System.out.println("IO problem" + e);
        }
        System.out.println(message);
        total = message.length();
        for (int i = 0; i < message.length(); i++) {
            if (message.toLowerCase().charAt(i) == 'e') {
                numE++;
            }

        }
        System.out.println("Number of e/E's: " + numE + " out of " + total);
    }
    public static void main(String[] args) {
        new ExerciseOne().start();
    }


}
