package ictgradschool.industry.io.ex02;

import ictgradschool.Keyboard;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;


public class MyReader {

    public void start() {
System.out.println("Enter a file name: ");
String userInput = Keyboard.readInput().trim();

        File myFile = new File(userInput);
        if (myFile.exists()){
            try (BufferedReader reader = new BufferedReader(new FileReader(myFile))){
                String line = null;
                while ((line = reader.readLine()) != null){
                    System.out.println(line);
                }
            }catch(IOException e){
                System.out.println("Yet another IO exception "+e);
            }

        }
        // TODO Prompt the user for a file name, then read and print out all the text in that file.
        // TODO Use a BufferedReader.
    }

    public static void main(String[] args) {
        new MyReader().start();
    }
}
