package ictgradschool.industry.io.ex02;

import ictgradschool.Keyboard;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class MyScanner {

    public void start() {
        System.out.println("Enter a file name: ");
        String userInput = Keyboard.readInput().trim();
        File myFile = new File(userInput);
        if (myFile.exists()) {
            try (Scanner scanner = new Scanner(myFile)) {

                while (scanner.hasNextLine()) {
                    System.out.println(scanner.nextLine());
                }
            } catch (IOException e) {
                System.out.println("Yet another IO exception " + e);
            }
        }
    }

    public static void main(String[] args) {
        new MyScanner().start();
    }
}
