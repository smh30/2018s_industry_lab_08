package ictgradschool.industry.io.ex03;


import ictgradschool.Keyboard;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

/**
 * Created by anhyd on 20/03/2017.
 */
public class MovieReader {

    public void start() {


        // Get a file name from the user
        System.out.print("Enter a file name: ");
        String fileName = Keyboard.readInput();

        // Load the movie data
        Movie[] films = loadMovies(fileName);

        // Do some stuff with the data to check that its working
        printMoviesArray(films);
        Movie mostRecentMovie = getMostRecentMovie(films);
        Movie longestMovie = getLongestMovie(films);
        printResults(mostRecentMovie, longestMovie);
        System.out.println();
        printDirector("Searching for Sugar Man", films);
        printDirector("Liberal Arts", films);
        printDirector("The Intouchables", films);

    }

    /**
     * Reads movies from a file.
     *
     * @param fileName
     * @return
     */
    protected Movie[] loadMovies(String fileName) {

        Movie[] films = null;

        try (DataInputStream in = new DataInputStream(new FileInputStream(fileName))) {


            int arrayLength = in.readInt();
            films = new Movie[arrayLength];


            for (int i = 0; i < films.length; i++) {
            String name = in.readUTF();
            int year = in.readInt();
            int length = in.readInt();
            String dir = in.readUTF();
//                    byte nameLength = (byte)fIn.read();
//                    byte[] nameArray = new byte[nameLength];
//                    for (int j = 0; j < nameArray.length; j++) {
//                        nameArray[j] = (byte)fIn.read();
//                    }
//                    byte yearLength = (byte)fIn.read();
//                    byte[] yearArray = new byte[yearLength];
//                    for (int j = 0; j < yearArray.length; j++) {
//                        yearArray[j] = (byte)fIn.read();
//                    }
//                    byte filmLength = (byte)fIn.read();
//                    byte[] lengthArray = new byte[filmLength];
//                    for (int j = 0; j < lengthArray.length; j++) {
//                        lengthArray[j] = (byte)fIn.read();
//                    }
//                    byte filmDir = (byte)fIn.read();
//                    byte[] dirArray = new byte[filmDir];
//                    for (int j = 0; j < dirArray.length; j++) {
//                        dirArray[j] = (byte)fIn.read();
            films[i] = new Movie(name, year, length, dir);
            }


//                    String name = new String(nameArray);
//                    int year = Integer.parseInt(new String(yearArray));
//                    int length = Integer.parseInt(new String(lengthArray));
//                    String director = new String(dirArray);


        } catch (IOException e) {
            System.out.println("fucking errors " + e);
        }


        System.out.println("Movies loaded successfully from " + fileName + "!");
        return films;

    }

    private void printMoviesArray(Movie[] films) {
        System.out.println("Movie Collection");
        System.out.println("================");
        // Step 2.  Complete the printMoviesArray() method
        for (int i = 0; i < films.length; i++) {
            System.out.println(films[i].toString());
        }
    }

    private Movie getMostRecentMovie(Movie[] films) {
        // Step 3.  Complete the getMostRecentMovie() method.
        Movie temp = null;
        for (int i = 1; i < films.length; i++) {
            if (films[i].isMoreRecentThan(films[i - 1])) {
                temp = films[i];
            }
        }
        return temp;
    }

    private Movie getLongestMovie(Movie[] films) {
        // Step 4.  Complete the getLongest() method.
        Movie temp = null;
        for (int i = 1; i < films.length; i++) {
            if (films[i].isLongerThan(films[i - 1])) {
                temp = films[i];
            }
        }
        return temp;
    }

    private void printResults(Movie mostRecent, Movie longest) {
        System.out.println();
        System.out.println("The most recent movie is: " + mostRecent.toString());
        System.out.println("The longest movie is: " + longest.toString());
    }

    private void printDirector(String movieName, Movie[] movies) {
        // Step 5. Complete the printDirector() method
        for (int i = 0; i < movies.length; i++) {
            if (movieName.equals(movies[i].getName())) {
                System.out.println(movieName + " was directed by " + movies[i].getDirector());
                return;
            }
        }
        System.out.println(movieName + " is not in the collection.");
    }

    public static void main(String[] args) {
        new MovieReader().start();
    }
}
