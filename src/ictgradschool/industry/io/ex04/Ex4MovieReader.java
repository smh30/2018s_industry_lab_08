package ictgradschool.industry.io.ex04;

import ictgradschool.industry.io.ex03.Movie;
import ictgradschool.industry.io.ex03.MovieReader;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Created by anhyd on 20/03/2017.
 */
public class Ex4MovieReader extends MovieReader {

    @Override
    protected Movie[] loadMovies(String fileName) {
        File myFile = new File(fileName);

        Movie[] films = new Movie[19];

        try (Scanner scanner = new Scanner(myFile)) {

            //todo this needs to be \\r\\n for windows i think??????
            scanner.useDelimiter(",|\\n");

            int i = 0;
            while (scanner.hasNext()) {
                String name = scanner.next();
                int year = scanner.nextInt();
                int length = scanner.nextInt();
               String dir = scanner.next();

                //System.out.println(name + " " + year + " " +  length + " " +dir );
                films[i] = new Movie(name, year, length, dir);
                i++;

            }

            //System.out.println(films.length);

        } catch(FileNotFoundException e){
            System.out.println("The file is lost in space *** " + e.getMessage());
        }

        return films;
    }

    public static void main(String[] args) {
        new Ex4MovieReader().start();
    }
}
