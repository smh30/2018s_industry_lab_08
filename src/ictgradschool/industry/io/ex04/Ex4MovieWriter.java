package ictgradschool.industry.io.ex04;

import ictgradschool.industry.io.ex03.Movie;
import ictgradschool.industry.io.ex03.MovieWriter;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by anhyd on 20/03/2017.
 */
public class Ex4MovieWriter extends MovieWriter {

    @Override
    protected void saveMovies(String fileName, Movie[] films) {

        try (PrintWriter writer = new PrintWriter(new FileWriter(fileName))) {
            for (int i = 0; i < films.length; i++) {
                writer.print(films[i].getName() + ',');
                writer.print(films[i].getYear());
                writer.print(',');
                writer.print(films[i].getLengthInMinutes());
                writer.print(',');
                writer.println(films[i].getDirector());
            }



            System.out.println("Movies saved successfully to " + fileName + "!");


        } catch (IOException e) {
            System.out.println("Error yet again: " + e.getMessage());
        }
    }

    public static void main(String[] args) {
        new Ex4MovieWriter().start();
    }

}
