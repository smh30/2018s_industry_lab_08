package ictgradschool.industry.io.ex05;

import ictgradschool.Keyboard;

import java.beans.IndexedPropertyDescriptor;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

public class UserCode extends CodeNumber {
    // field declarations
    // has int[] codenumber from superclass
    private static boolean manual = false;

    //constructor:


    public UserCode(String type, List guesses, int iterations) {

        boolean x = false;
        while (!x) {
            String line = "";

            try {
                line = guesses.remove(0).toString();
            } catch (IndexOutOfBoundsException e) {
                //if it's the first go and there's no guesses, do manuall
                if (type.equals("secret code")) {
                    line = getUserInput(type);
                } else if (iterations == 0 || manual) {
                    line = getUserInput(type);
                    manual = true;
                    //if it's not the first go, and we've been auto till this point...
                } else if (iterations > 0) {
                    System.out.println("No more auto-guesses left");
                    System.out.println("Please guess manually");
                    line = getUserInput(type);
                    manual = true;
                }
            }


            try {
                int[] temp = arrayify(line);
                if (checkValid(temp)) {
                    for (int i = 0; i < codeNumber.length; i++) {
                        codeNumber[i] = temp[i];
                    }
                    x = true;
                    return;

                } else {
                    System.out.println("invalid number, must be 4 unique digits, try again");
                }
            } catch (NumberFormatException e) {
                System.out.println("That wasn't a number, try again:");

            } catch (StringIndexOutOfBoundsException e) {
                System.out.println("That number is too short, try again:");

            }
        }
    }


    public int[] arrayify(String input) {
        int[] temp = new int[4];
        for (int i = 0; i < temp.length; i++) {
            temp[i] = Integer.parseInt(String.valueOf(input.charAt(i)));
        }
        return temp;
    }

    // the 'type' is whether it's a secret code or a guess
    public String getUserInput(String type) {
        System.out.println("Please enter your " + type + ": (4 unique digits, no spaces): ");
        return Keyboard.readInput();
    }


// i think this method was just for testing. leaving it until i'm sure
    /*public String toString(int[] array) {
        String str = "";
        for (int i = 0; i < array.length; i++) {
            str += array[i];
        }
        return str;
    }*/


}
